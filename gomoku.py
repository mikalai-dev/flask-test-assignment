# This file contains logic for gomoku game - XO game with 15x15 field
import json
import copy

def get (board, row, col, symbol):
    """
    Method for safe getting board cell value
    """
    if row < 0 or row >= 15 or col < 0 or col >= 15:
        return 0
    return board[row][col]


def copy_board(board):
    """
    Method for making copy of board object for winning combinations checks
    """
    return copy.deepcopy(board)


def get_opposite_symbol(symb):
    """
    Method for quick switching between human player symbol (X or 0) and
    computer one
    """
    if symb == 'X':
        return '0'
    elif symb == '0':
        return 'X'


def check_win(board, symbol):
    """
    This method checks the bord for 5 symbols in on line
    (vertical, horizontal or diagonal)
    """
    dirs = ((1, -1), (1, 0), (1, 1), (0, 1))
    for i in range(15):
        for j in range(15):
            if board[i][j] != symbol: continue
            id = board[i][j]
            for d in dirs:
                x, y = j, i
                count = 0
                for k in range(5):
                    if get(board, y, x, symbol) != id: break
                    y += d[0]
                    x += d[1]
                    count += 1
                if count == 5:
                    return True
    return False


def make_human_turn(board, symbol, i, j):
    """
    This method place symbol to a board cell if it is empty
    """
    if board[i][j] not in ['X', '0']:
        board[i][j] = symbol


def make_computer_turn(board, symbol):
    """
    Check for posibility to win in one move and
    occupy this position
    """
    board_copy = copy_board(board)
    for i in range(15):
        for j in range(15):
            board_copy = copy_board(board)
            if board_copy[i][j] == None:
                board_copy[i][j] = symbol
                if check_win(board_copy, symbol):
                    board[i][j] = symbol
                    return (i, j)

    """
    Check if player can win in one move and
    occupy this position
    """
    player_symbol = get_opposite_symbol(symbol)
    for i in range(15):
        for j in range(15):
            board_copy = copy_board(board)
            if board_copy[i][j] == None:
                board_copy[i][j] = player_symbol
                if check_win(board_copy, player_symbol):
                    board[i][j] = symbol
                    return (i, j)

    """
    Check availability of central cells
    """
    for i in range(5, 10):
        for j in range(5, 10):
            board_copy = copy_board(board)
            if board_copy[i][j] == None:
                board[i][j] = symbol
                return (i, j)


    """
    Check availability of corner cells
    """
    for i in [0, 14]:
        for j in [0, 14]:
            if board_copy[i][j] == None:
                board[i][j] = symbol
                return (i, j)

    """
    Find available cells on board sides
    """
    for i in [0, 1, 2, 12, 13, 14]:
        for j in range(0, 15):
            if board_copy[j][i] == None:
                board[j][i] = symbol
                return (i, j)

    return (None, None)
