FROM python:3.8
WORKDIR /app
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt-get -y update
COPY ./requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
