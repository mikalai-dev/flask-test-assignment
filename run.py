from os import environ, path
from flask import Flask, jsonify, request, g
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_httpauth import HTTPBasicAuth
from models import User
from views import *

from exts import app, db, auth
migrate = Migrate(app,db)

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

@app.route('/api/user/signup', methods=['POST'])
def signup():
    args = request.get_json()
    if args is not None:
       if 'username' in args and 'password' in args:
           user = User(username=args['username'])
           user.hash_password(args['password'])
           db.session.add(user)
           db.session.commit()
           return jsonify({'id': user.id})
    return jsonify({'message': "username and password are required"})

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
