# XO Game Rest API project

## Description:
This project provide backend for XO (gomoku) game single-player application.

## Prerequisites

Before deploying this project update **.env** file in the root with appropriate values for database name, database user, database user password and project secret key

Make sure, that docker and docker-compose are installed on a target host.

For debian based systems run
```
sudo apt-get install docker.io docker-compose
```

## Deployment

Clone this repo and run

```
docker-compose up -d
```

## Using API

### Before starting the game sign up on the service with username and password:
```
curl -u User:123456 -i -X GET 'http://localhost/api/token'

```

In repsponse you will get auth token for performing other actions in a game

```
HTTP/1.1 200 OK                                                              
Server: nginx/1.19.7
Date: Tue, 03 Mar 2021 12:00:00 GMT
Content-Type: application/json
Content-Length: 178
Connection: keep-alive

{"token":"eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxNDcxNTc3NiwiZXhwIjoxNjE0NzE5Mzc2fQ.eyJpZCI6MX0.cU_1PmDWyZELfpWiU87onERMzUs0woLgEPuC-bPoTkwHFa5MvbsbETrlCmeP1UIH3wVuNzqi9IMz4X9uTQIf_w"}

```

## To start a new game call the /api/start endpoint:

```bash
$ curl -u eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxNDcxNTc3NiwiZXhwIjoxNjE0NzE5Mzc2fQ.eyJpZCI6MX0.cU_1PmDWyZELfpWiU87onERMzUs0woLgEPuC-bPoTkwHFa5MvbsbETrlCmeP1UIH3wVuNzqi9IMz4X9uTQIf_w:unused -i -H "Content-Type: application/json"-X POST  'http://localhost/api/game/start'
```
A response will contain field values and id of created game. A new field can be empty or with a first move made by computer - it depends on random choice. Symbols for players assigned randomly

```curl -u eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxNDcxNTc3NiwiZXhwIjoxNjE0NzE5Mzc2fQ.eyJpZCI6MX0.cU_1PmDWyZELfpWiU87onERMzUs0woLgEPuC-bPoTkwHFa5MvbsbETrlCmeP1UIH3wVuNzqi9IMz4X9uTQIf_w:unused -i -X POST 'http://localhost/api/game/start'
HTTP/1.1 200 OK                                                    
Server: nginx/1.19.7
Date: Tue, 03 Mar 2021 12:00:00 GMT
Content-Type: application/json
Content-Length: 1179
Connection: keep-alive

{"board":[[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,"0",null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],
"game_id":1}

```

## To make a move use /api/game/make_move endpoint

Parameters should contain game_id and coordinates for moving onto an empty cell

```
$ curl -u eyJhbGciOiJIUzUxMiIsImlhdCI6MTYxNDcxNTc3NiwiZXhwIjoxNjE0NzE5Mzc2fQ.eyJpZCI6MX0.cU_1PmDWyZELfpWiU87onERMzUs0woLgEPuC-bPoTkwHFa5MvbsbETrlCmeP1UIH3wVuNzqi9IMz4X9uTQIf_w:unused -i -H "Content-Type: application/json" -X POST 'http://localhost/api/game/make_move' -d '{"game_id": 1, "i": 0, "j": 0}'
```

Service reponse will contain updated board values

```
HTTP/1.1 200 OK
Server: gunicorn/20.0.4
Date: Tue, 03 Mar 2021 12:00:00 GMT
Connection: close
Content-Type: application/json
Content-Length: 1164

{"board":[["X",null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,"0","0",null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]]}

```

In a case when you win or lose on any turn, service will response appropriate message

```
{'message': 'Gongrats! You have won!'}
```

or

```
{'message': 'Sorry, you lose this time...'}
```
