from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask_httpauth import HTTPBasicAuth

"""
Flask definitions for eliminating from circular imports
"""
app = Flask(__name__)
app.config.from_object('config.ProdConfig')

db = SQLAlchemy(app)
auth = HTTPBasicAuth()
