import json
import random
from flask import jsonify, request, g
from exts import app, db, auth
from models import User, Game
from gomoku import *

@app.route('/api/token')
@auth.login_required
def get_auth_token():
    """
    Auth token generation after password authentication
    """
    token = g.user.generate_auth_token()
    return jsonify({ 'token': token.decode('ascii') })

@app.route('/api/game/start', methods=['POST'])
@auth.login_required
def start_game():
    """
    New game creation. Allowed to a logged user only
    """
    user = g.user
    board = [[None]* 15 for i in range(15)]

    #Chosing random symbol for human player
    symbol = random.choice('X0')
    #Computer starts game in a random case
    if random.choice([True, False]):
        i, j = make_computer_turn(board, symbol)

    # Create game object with randomly chosen player symbol (X or 0)
    game = Game(name='test', description="blah", board=json.dumps(board),
                user_id=user.id, user_symbol=random.choice('X0'))


    db.session.add(game)
    db.session.commit()
    return jsonify({'game_id': game.id, 'board': board})

@app.route('/api/game/make_move', methods=['POST'])
@auth.login_required
def make_move():
    """
    Method for making move by human player
    """
    request_data = request.get_json()
    game_id = request_data.get('game_id', None)
    i = request_data.get('i', None)
    j = request_data.get('i', None)
    if game_id is None:
        return jsonify({'messsage': 'Please, specify game id to make move'})
    if i is None:
        return jsonify({'messsage': 'Please, specify coordinates for moving'})
    if j is None:
        return jsonify({'messsage': 'Please, specify coordinates for moving'})

    game = None
    if Game.query.filter_by(id=game_id).first():
        game = Game.query.get(game_id)
    else:
        return jsonify({'message': 'This game does not exists'})

    user = g.user
    if (user.id != game.user_id):
        return jsonify({'messsage': 'Your are not allowed to play on this board'})
    else:
        try:
            i = int(i)
            j = int(j)
        except:
            return jsonify({'messsage': 'Coordinates values should be integer numbers'})

        board = json.loads(game.board)
        make_human_turn(board, game.user_symbol, int(i), int(j))
        game.board = json.dumps(board)
        db.session.add(game)
        db.session.commit()

        # Checking if previous move led to victory
        if check_win(board, game.user_symbol):
            return jsonify({'message': 'Gongrats! You have won!'})

        # Checking if previous move led to defeat
        if check_win(board, get_opposite_symbol(game.user_symbol)):
            return jsonify({'message': 'Sorry, you lose this time...'})

        # Performing return move by a computer
        make_computer_turn(board, get_opposite_symbol(game.user_symbol))
        return jsonify({'board': board})
